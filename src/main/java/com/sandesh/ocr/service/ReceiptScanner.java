/**
 * 
 */
package com.sandesh.ocr.service;

/**
 * @author Sandesh Keshaowar (46001464)
 *
 */
public interface ReceiptScanner {

	
	/**
	 * @param receiptImageFilePath
	 * @return
	 */
	public String getTextFromReceiptImage(final String receiptImageFilePath);
}
