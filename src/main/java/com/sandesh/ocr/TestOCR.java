package com.sandesh.ocr;

import com.sandesh.ocr.service.ReceiptScanner;
import com.sandesh.ocr.service.impl.ReceiptScannerImpl;

public class TestOCR {

	public static void main(String[] args) {
		final String receiptImageFilePath = "C:\\Users\\skeshaow\\Desktop\\receipts\\receipt-1.jpg";
		ReceiptScanner scanner = new ReceiptScannerImpl();
		String result = scanner.getTextFromReceiptImage(receiptImageFilePath);
		
		System.out.println(result);
	}

}
